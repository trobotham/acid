/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class VizonUser
{
	private static final Logger logger = LoggerFactory.getLogger(VizonUser.class);

	private final int id;
	private final String nick;
	private String vhost;
	private boolean eligible;
	private boolean bold;
	private LocalDateTime obtained;
	private int obtainedId;
	private int expires;
	private int multiplier;
	private boolean jackpot;
	private boolean permanent;
	private int days;

	public static VizonUser fromResultSet(ResultSet rs)
	{
		try
		{
			int id = rs.getInt("id");
			String nick = rs.getString("nick");
			String vhost = rs.getString("vhost");
			boolean eligible = rs.getBoolean("eligible");
			boolean bold = rs.getBoolean("bold");
			int expires = rs.getInt("expires");
			Timestamp obtained = rs.getTimestamp("obtained");
			int obtainedId = rs.getInt("obtained_id");
			int multiplier = rs.getInt("multiplier");
			boolean jackpot = rs.getBoolean("jackpot");
			boolean permanent = rs.getBoolean("permanent");
			int days = rs.getInt("days");

			LocalDateTime date = null;

			if (obtained != null)
			{
				date = obtained.toLocalDateTime();
			}

			return new VizonUser(
					id,
					nick,
					vhost,
					eligible,
					bold,
					date,
					obtainedId,
					expires,
					multiplier,
					jackpot,
					permanent,
					days);
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to construct VizonUser from ResultSet", ex);
			return null;
		}
	}


	private VizonUser(
			int id,
			String nick,
			String vhost,
			boolean eligible,
			boolean bold,
			LocalDateTime obtained,
			int obtainedId,
			int expires,
			int multiplier,
			boolean jackpot,
			boolean permanent,
			int days)
	{
		this.id = id;
		this.nick = nick;
		this.vhost = vhost;
		this.eligible = eligible;
		this.bold = bold;
		this.obtained = obtained;
		this.obtainedId = obtainedId;
		this.expires = expires;
		this.multiplier = multiplier;
		this.jackpot = jackpot;
		this.permanent = permanent;
		this.days = days;
	}

	public int getId()
	{
		return id;
	}

	public String getNick()
	{
		return nick;
	}

	public String getVhost()
	{
		return vhost;
	}

	public boolean isEligible()
	{
		return eligible;
	}

	public boolean isBold()
	{
		return bold;
	}

	public LocalDateTime getObtained()
	{
		return obtained;
	}

	public int getExpires()
	{
		return expires;
	}

	public boolean isJackpot()
	{
		return jackpot;
	}

	public boolean isPermanent()
	{
		return permanent;
	}

	public int getDays()
	{
		return days;
	}

	public void resetDays()
	{
		this.days = 0;
	}

	public void addDays(int days)
	{
		this.days += days;
	}

	public int getMultiplier()
	{
		return multiplier;
	}

	public void incrementMultiplier()
	{
		multiplier++;
	}

	public void resetMultiplier()
	{
		multiplier = 0;
	}

	public void setBold(boolean bold)
	{
		this.bold = bold;
	}

	public void setVhost(String vhost)
	{
		this.vhost = vhost;
	}

	public void setEligible(boolean eligible)
	{
		this.eligible = eligible;
	}

	public void setObtained(LocalDateTime obtained)
	{
		this.obtained = obtained;
	}

	public void setExpires(int expires)
	{
		this.expires = expires;
	}

	public void setJackpot(boolean jackpot)
	{
		this.jackpot = jackpot;
	}

	public void setPermanent(boolean permanent)
	{
		this.permanent = permanent;
	}

	public int getObtainedId()
	{
		return this.obtainedId;
	}

	public void setObtainedId(int drawingId)
	{
		this.obtainedId = drawingId;
	}

}
