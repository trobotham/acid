/*
 * Copyright (c) 2016, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Holds a user's bet.
 *
 * @author orillion <orillion@rizon.net>
 */
public class Bet
{
	private final int first;
	private final int second;
	private final int third;
	private final int fourth;
	private final int fifth;
	private final int sixth;

	/**
	 * Constructs an object that holds a user's bet.
	 *
	 * @param first  First number the user bet
	 * @param second Second number the user bet
	 * @param third  Third number the user bet
	 * @param fourth Fourth number the user bet
	 * @param fifth  Fifth number the user bet
	 * @param sixth  Sixth number the user bet
	 */
	public Bet(int first, int second, int third, int fourth, int fifth, int sixth)
	{
		this.first = first;
		this.second = second;
		this.third = third;
		this.fourth = fourth;
		this.fifth = fifth;
		this.sixth = sixth;
	}

	/**
	 * Gets the first number the user bet.
	 *
	 * @return First number
	 */
	public int getFirst()
	{
		return first;
	}

	/**
	 * Gets the second number the user bet.
	 *
	 * @return Second number
	 */
	public int getSecond()
	{
		return second;
	}

	/**
	 * Gets the third number the user bet.
	 *
	 * @return Third number
	 */
	public int getThird()
	{
		return third;
	}

	/**
	 * Gets the fourth number the user bet.
	 *
	 * @return Fourth number
	 */
	public int getFourth()
	{
		return fourth;
	}

	/**
	 * Gets the fifth number the user bet.
	 *
	 * @return Fifth number
	 */
	public int getFifth()
	{
		return fifth;
	}

	/**
	 * Gets the sixth number the user bet.
	 *
	 * @return Sixth number
	 */
	public int getSixth()
	{
		return sixth;
	}

	public List<Integer> asList()
	{
		return Arrays.asList(
				first,
				second,
				third,
				fourth,
				fifth,
				sixth);
	}

	public boolean hasSameNumbers(Bet other)
	{
		Set<Integer> ours = new HashSet<>(asList()),
			theirs = new HashSet<>(other.asList());
		return ours.equals(theirs);
	}
}
