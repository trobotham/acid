/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon;

import net.rizon.acid.plugins.vizon.db.VizonRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import net.rizon.acid.conf.Config;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class RequestTracker
{
	public static final String MESSAGE_FORMAT = "[new] %d. %s :: %s\u000F :: Vizon vhost";
	private static final String REJECT_FORMAT = "SEND %s Your colored vhost %s\u000F has been rejected";
	private static final String APPROVE_FORMAT = "SEND %s Your colored vhost %s\u000F has been approved";
	private final List<VizonRequest> requests = new ArrayList<>();
	private final Config config;

	public RequestTracker(Config conf)
	{
		this.config = conf;
	}

	public void addRequest(VizonRequest request)
	{
		Optional<VizonRequest> req = this.requests
				.stream()
				.filter(r -> r != null)
				.filter(r -> r.getId() == request.getId())
				.findFirst();

		int index = 0;

		if (req.isPresent())
		{
			index = this.requests.indexOf(req.get());
			// Replace old request with new one
			this.requests.set(index, request);
		}
		else
		{
			// Add new request to list.
			this.requests.add(request);

			index = this.requests.size() - 1;
		}

		String channel = this.config.getChannelNamed("vhost");
		Channel c = Channel.findChannel(channel);
		
		if (c == null)
			return;

		Acidictive.reply(
				null,
				Vizon.getVizonBot(),
				c,
				String.format(
						MESSAGE_FORMAT,
						index + 1,
						request.getNick(),
						request.getVhost()));
	}

	public boolean approveRequest(int request, String oper)
	{
		if (request < 0 || request >= this.requests.size())
		{
			return false;
		}

		VizonRequest req = this.requests.get(request);

		if (req == null)
		{
			return false;
		}

		req.setOper(oper);
		req.setStatus(RequestStatus.APPROVED);

		if (!Vizon.getVhostManager().assignVhost(req))
		{
			return false;
		}

		if (!Vizon.getVizonDatabase().updateRequest(req))
		{
			return false;
		}

		Acidictive.privmsg(
				Vizon.getVizonBot().getNick(),
				"MemoServ",
				String.format(
						APPROVE_FORMAT,
						req.getNick(),
						req.getVhost()));

		this.requests.set(request, null);
		this.trimList();

		return true;
	}

	public boolean rejectRequest(int request, String oper, String reason)
	{
		VizonRequest req = this.requests.get(request);

		if (req == null)
		{
			return false;
		}

		req.setOper(oper);
		req.setReason(reason);
		req.setStatus(RequestStatus.REJECTED);

		if (!Vizon.getVizonDatabase().updateRequest(req))
		{
			return false;
		}

		Acidictive.privmsg(
				Vizon.getVizonBot().getNick(),
				"MemoServ",
				String.format(
						REJECT_FORMAT,
						req.getNick(),
						req.getVhost()));

		this.requests.set(request, null);
		this.trimList();

		return true;
	}

	private void trimList()
	{
		for (int index = this.requests.size() - 1; index >= 0; index--)
		{
			if (this.requests.get(index) != null)
			{
				// Clean up list until we find a non-null entry.
				break;
			}

			this.requests.remove(index);
		}
	}

	public void approveAllRequests(String oper)
	{
		for (int i = 0; i < this.requests.size(); i++)
		{
			this.approveRequest(i, oper);
		}
	}

	public void rejectAllRequests(String oper)
	{
		for (int i = 0; i < this.requests.size(); i++)
		{
			this.rejectRequest(i, oper, null);
		}
	}

	public void approveAllRequestsExcept(List<Integer> reqs, String oper)
	{
		for (int i = 0; i < this.requests.size(); i++)
		{
			if (reqs.contains(i))
			{
				this.approveRequest(i, oper);
			}
			else
			{
				this.rejectRequest(i, oper, null);
			}
		}
	}

	public List<VizonRequest> getPending()
	{
		Collection<VizonRequest> reqs = Vizon
				.getVizonDatabase()
				.findPendingVhostRequests();

		if (reqs != null)
		{
			for (VizonRequest req : reqs)
			{
				Optional<VizonRequest> request = this.requests
						.stream()
						.filter(r -> r != null)
						.filter(r -> r.getId() == req.getId())
						.findFirst();

				if (request.isPresent())
				{
					int index = this.requests.indexOf(request.get());
					// Replace old request with new one
					this.requests.set(index, req);
				}
				else
				{
					// Add new request to list.
					this.requests.add(req);

				}
			}
		}

		return Collections.unmodifiableList(this.requests);
	}
}
