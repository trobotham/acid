package net.rizon.acid.plugins.trapbot;

import java.util.Date;

class TrappedUser
{
	public String ip;
	public Date lastTrap;

	TrappedUser(String ip)
	{
		this.ip = ip;
	}

	public void update()
	{
		lastTrap = new Date();
	}
}