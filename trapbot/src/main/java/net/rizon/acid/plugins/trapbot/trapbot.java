package net.rizon.acid.plugins.trapbot;

import com.google.common.eventbus.Subscribe;
import io.netty.util.concurrent.ScheduledFuture;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.plugins.Plugin;
import net.rizon.acid.core.Protocol;
import net.rizon.acid.core.User;
import net.rizon.acid.events.EventJoin;
import net.rizon.acid.events.EventKick;
import net.rizon.acid.events.EventPart;
import net.rizon.acid.events.EventUserConnect;
import net.rizon.acid.plugins.trapbot.conf.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class trapbot extends Plugin
{
	protected static final Logger log = LoggerFactory.getLogger(trapbot.class);

	public static AcidUser trapbot;
	public static Config conf;
	private ScheduledFuture expireTimer;
	public static ScheduledFuture retrapTimer;
	public static ScheduledFuture releaseTimer;
	// ip -> user
	public static HashMap<String, TrappedUser> users = new HashMap<String, TrappedUser>();
	public static boolean enforce = true;

	public static String getTrapChanName()
	{
		return Acidictive.conf.getChannelNamed(conf.trapchan);
	}

	public static TrappedUser makeTrappedUser(User u)
	{
		TrappedUser tu = new TrappedUser(u.getIP());
		users.put(u.getIP(), tu);
		return tu;
	}

	public static TrappedUser getTrappedUser(User u)
	{
		if (u.getIP() == null || u.getIP().equals("0"))
			return null;

		return users.get(u.getIP());
	}

	public static void freeUser(User u)
	{
		users.remove(u.getIP());
	}

	public static void updateTrap(User u)
	{
		if (u.getServer() == AcidCore.me || u.getServer().isUlined())
			return;

		/* ignore spoofed users */
		if (u.getIP() == null || u.getIP().equals("0") || u.getIP().equals("255.255.255.255"))
			return;

		TrappedUser t = getTrappedUser(u);

		if (t == null)
			t = makeTrappedUser(u);

		t.update();
	}

	@Override
	public void start() throws Exception
	{
		Acidictive.eventBus.register(this);

		reload();

		expireTimer = Acidictive.scheduleWithFixedDelay(new ExpireTimer(), 10, TimeUnit.MINUTES);

		// magic!
		releaseTimer = Acidictive.schedule(new ReleaseTimer(), new Random().nextInt(2700) + 2700, TimeUnit.SECONDS);
	}

	@Override
	public void reload() throws Exception
	{
		conf = (Config) net.rizon.acid.conf.Config.load("trapbot.yml", Config.class);
		Acidictive.loadClients(this, conf.clients);

		User u = User.findUser(conf.trapbot);
		if (u == null || !(u instanceof AcidUser))
			throw new Exception("Unable to find trapbot " + conf.trapbot);

		trapbot = (AcidUser) u;
	}

	@Override
	public void stop()
	{
		Acidictive.eventBus.unregister(this);

		if (expireTimer != null)
			expireTimer.cancel(true);

		if (releaseTimer != null)
			releaseTimer.cancel(true);

		if (retrapTimer != null)
			retrapTimer.cancel(true);
	}

	@Subscribe
	public void onUserConnect(EventUserConnect event)
	{
		User u = event.getUser();

		TrappedUser t = getTrappedUser(u);
		/* force the user back in */
		if (t != null)
		{
			t.update();
			Protocol.svsjoin(u, getTrapChanName());
		}
	}

	@Subscribe
	public void onJoin(EventJoin event)
	{
		Channel channel = event.getChannel();
		User[] users = event.getUsers();

		if (!channel.getName().equalsIgnoreCase(getTrapChanName()))
			return;

		for (User u : users)
		{
			updateTrap(u);
		}
	}

	@Subscribe
	public void onPart(EventPart event)
	{
		Channel channel = event.getChannel();
		User u = event.getUser();

		if (!channel.getName().equalsIgnoreCase(getTrapChanName()))
			return;

		if (u.hasMode("o"))
			return;

		TrappedUser tu = getTrappedUser(u);
		if (tu == null)
			tu = makeTrappedUser(u);

		/*
		 * if enforce is disabled, we let the parting users go
		 */
		if (enforce)
		{
			tu.update();
			Protocol.svsjoin(u, getTrapChanName());
		}
		else
			freeUser(u);
	}

	@Subscribe
	public void onKick(EventKick event)
	{
		Channel channel = event.getChannel();
		User victim = event.getVictim();

		if (!channel.getName().equalsIgnoreCase(getTrapChanName()))
			return;

		freeUser(victim);
	}
}
