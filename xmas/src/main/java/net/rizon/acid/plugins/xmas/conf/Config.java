package net.rizon.acid.plugins.xmas.conf;

import java.util.List;
import net.rizon.acid.conf.ConfigException;
import net.rizon.acid.conf.Configuration;
import net.rizon.acid.conf.Validator;

public class Config extends Configuration
{
	public String xmaschan;
	public String timezone;
	public String topic;
	public int specialminutes;
	public List<VhostDay> vhosts;

	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotEmpty("xmaschan", xmaschan);
		Validator.validateNotEmpty("timezone", timezone);
		Validator.validateNotEmpty("topic", topic);
		Validator.validateNotZero("specialminutes", specialminutes);
	}
}
