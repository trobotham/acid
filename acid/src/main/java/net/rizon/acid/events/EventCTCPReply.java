package net.rizon.acid.events;

import net.rizon.acid.core.User;

public class EventCTCPReply
{
	private User source;
	private String target;
	private String message;

	public User getSource()
	{
		return source;
	}

	public void setSource(User source)
	{
		this.source = source;
	}

	public String getTarget()
	{
		return target;
	}

	public void setTarget(String target)
	{
		this.target = target;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
}
