package net.rizon.acid.events;

import net.rizon.acid.core.Channel;

public class EventChanMode
{
	private String prefix;
	private Channel chan;
	private String modes;

	public String getPrefix()
	{
		return prefix;
	}

	public void setPrefix(String prefix)
	{
		this.prefix = prefix;
	}

	public Channel getChan()
	{
		return chan;
	}

	public void setChan(Channel chan)
	{
		this.chan = chan;
	}

	public String getModes()
	{
		return modes;
	}

	public void setModes(String modes)
	{
		this.modes = modes;
	}
}
