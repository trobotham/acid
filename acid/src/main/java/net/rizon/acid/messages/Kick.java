package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Kick extends Message
{
	private static final Logger log = LoggerFactory.getLogger(Kick.class);

	public Kick()
	{
		super("KICK");
	}

	// :99hAAAAAB KICK #geo 99hAAAAAB :Lame and fake

	@Override
	public void on(String source, String[] params)
	{
		String channel = params[0];
		User kickee = User.findUser(params[1]);
		if (kickee == null)
		{
			log.warn("KICK for nonexistent user " + params[1]);
			return;
		}

		String kicker = User.toName(source);;
		if (kicker == source)
			kicker = Server.toName(source);
		if (kicker == source)
			log.warn("KICK from nonexitent source " + source);

		Channel chan = Channel.findChannel(channel);
		if (chan == null)
		{
			log.warn("KICK from " + kicker + " for " + kickee.getNick() + " on nonexistent channel " + channel);
			return;
		}

		chan.removeUser(kickee);
		kickee.remChan(chan);

		Acidictive.onKick(kicker, kickee, chan, params[2]);
	}
}