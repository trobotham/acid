package net.rizon.acid.arguments;

import java.time.Duration;

/**
 * ExpiryArgument accepts a String representation of a time span in the format
 * of "\+\d+[s|m|h|d|w|y]".
 * @author orillion
 */
public class ExpiryArgument
{
	private final Duration duration;
	private final String argument;

	private ExpiryArgument(String arg, Duration duration)
	{
		this.argument = arg;
		this.duration = duration;
	}

	/**
	 * Creates a new {@link ExpiryArgument} object.
	 * <p>
	 * @param arg	String that represents an {@link ExpiryArgument}, for
	 *		example:<br>
	 *		- "+30d"<br>
	 *		- "+4w"
	 * <p>
	 *		valid options for are:<br>
	 *		- s (seconds)<br>
	 *		- m (minutes)<br>
	 *		- h (hours)<br>
	 *		- d (days)<br>
	 *		- w (weeks)<br>
	 *		- y (years)
	 * <p>
	 *		Argument must start with "+".
	 * <p>
	 * @return	{@link ExpiryArgument} that represents the String, or
	 *		<code>null</code>
	 */
	public static ExpiryArgument parse(String arg)
	{
		if (arg == null || arg.isEmpty())
		{
			return null;
		}

		Duration duration;
		// +30d, cut out the d.
		String timeIdentifier = arg.substring(arg.length() - 1);
		// +30d, cut out the 30.
		String time = arg.substring(1, arg.length() - 1);

		if (!arg.startsWith("+"))
		{
			return null;
		}

		int multiplier = calculateMultiplier(timeIdentifier);

		if (multiplier == -1)
		{
			return null;
		}

		try
		{
			duration = Duration.ofSeconds(Long.parseLong(time) * multiplier);
		}
		catch (NumberFormatException e)
		{
			return null;
		}

		if (duration.isNegative() || duration.isZero())
		{
			return null;
		}

		return new ExpiryArgument(arg, duration);
	}

	private static int calculateMultiplier(String timeIdentifier)
	{
		switch (timeIdentifier)
		{
			case "s": return 1;
			case "m": return 60;
			case "h": return 3600;
			case "d": return 86400;
			case "w": return 604800;
			case "y": return 31536000;
			default: return -1;
		}
	}

	public Duration getDuration()
	{
		return this.duration;
	}

	public String getArgument()
	{
		return this.argument;
	}
}
