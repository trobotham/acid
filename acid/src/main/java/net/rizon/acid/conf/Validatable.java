package net.rizon.acid.conf;

public interface Validatable
{
	public void validate() throws ConfigException;
}