package net.rizon.acid.conf;

@SuppressWarnings("serial")
public class ConfigException extends Exception
{
	public ConfigException(String what)
	{
		super(what);
	}
}