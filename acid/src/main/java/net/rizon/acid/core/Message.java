package net.rizon.acid.core;

import java.util.HashMap;
import net.rizon.acid.plugins.ClassLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Message
{
	private static final Logger log = LoggerFactory.getLogger(Message.class);

	public static final char BOLD = 2;

	private static final String messageBase = "net.rizon.acid.messages";
	private static final String[] messageClasses = {
		"Encap", "EOB", "Error", "Join", "Kick", "Kill", "Mode", "Nick",
		"Notice", "Part", "Pass", "Ping", "Privmsg", "Quit", "Server", "SID",
		"SJoin", "SQuit", "Stats", "TMode", "UID", "Whois", "Operwall", "Invite",
	};

	static
	{
		reload();
	}

	protected Message(String name)
	{
		messages.put(name.toUpperCase(), this);
	}

	public void onUser(User source, String[] params) { }
	public void onServer(Server source, String[] params) { }
	public void on(String source, String[] params) { }

	private static HashMap<String, Message> messages;

	public static Message findMessage(String name)
	{
		return messages.get(name.toUpperCase());
	}

	public static void reload()
	{
		messages = new HashMap<String, Message>();

		try
		{
			ClassLoader loader = new ClassLoader("net.rizon.acid.messages.");

			for (String s : messageClasses)
				loader.loadClass(messageBase + "." + s).newInstance();
		}
		catch (Exception ex)
		{
			log.error("Error initializing messages", ex);
			System.exit(-1);
		}
	}
}
