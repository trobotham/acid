import re
from datetime import datetime

def format_last_req(last_req, message):
	c = '09'
	if last_req:
		d = datetime.now() - last_req.date
		if d.days < 1:
			c = '04'
		elif d.days < 7:
			c = '08'
	
	message = re.sub('^@b(\d+)\.@b(.*)', r'@b@c%s\1.@c@b\2' % c, message)
	return message.replace('@sep', '@b@c%s::@o' % c).replace('[new]', '@b@c%s[new]' % c)
