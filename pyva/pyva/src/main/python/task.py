import time
import logging

timers = []

def TimerCompare(t1, t2):
	if t1.tick < t2.tick:
		return -1
	elif t1.tick > t2.tick:
		return 1
	else:
		return 0

class Task():
	_log = logging.getLogger(__name__)
	secs = 0
	tick = 0
	repeating = None
	func = None

	def start(self, secs, repeating = True):
		self.secs = secs
		self.tick = int(time.time()) + secs
		self.repeating = repeating
		global timers
		if not timers.count(self):
			timers.append(self)
			timers = sorted(timers, cmp=TimerCompare)
	
	def stop(self):
		try:
			global timers
			timers.remove(self)
		except:
			pass
	
	def run(self, ts):
		self._log.debug("Running timer " + str(self.func))
		self.func()

		if not self.repeating:
			self.stop()
		else:
			global timers
			self.tick = ts + self.secs
			timers = sorted(timers, cmp=TimerCompare)

def LoopingCall(func):
	t = Task()
	t.func = func
	return t

def Run():
	now = int(time.time())

	global timers
	while len(timers) > 0 and timers[0].tick <= now:
		timers[0].run(now)
