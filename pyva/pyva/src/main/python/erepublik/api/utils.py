from decimal import Decimal, ROUND_DOWN, ROUND_UP
import math

__ranks = [
	{'name': 'Recruit',              'id': 1,  'points': 16},
	{'name': 'Private',              'id': 2,  'points': 46},
	{'name': 'Private*',             'id': 3,  'points': 81},
	{'name': 'Private**',            'id': 4,  'points': 121},
	{'name': 'Private***',           'id': 5,  'points': 171},
	{'name': 'Corporal',             'id': 6,  'points': 251},
	{'name': 'Corporal*',            'id': 7,  'points': 351},
	{'name': 'Corporal**',           'id': 8,  'points': 451},
	{'name': 'Corporal***',          'id': 9,  'points': 601},
	{'name': 'Sergeant',             'id': 10, 'points': 801},
	{'name': 'Sergeant*',            'id': 11, 'points': 1001},
	{'name': 'Sergeant**',           'id': 12, 'points': 1401},
	{'name': 'Sergeant***',          'id': 13, 'points': 1851},
	{'name': 'Lieutenant',           'id': 14, 'points': 2351},
	{'name': 'Lieutenant*',          'id': 15, 'points': 3001},
	{'name': 'Lieutenant**',         'id': 16, 'points': 3751},
	{'name': 'Lieutenant***',        'id': 17, 'points': 5001},
	{'name': 'Captain',              'id': 18, 'points': 6501},
	{'name': 'Captain*',             'id': 19, 'points': 9001},
	{'name': 'Captain**',            'id': 20, 'points': 12001},
	{'name': 'Captain***',           'id': 21, 'points': 15501},
	{'name': 'Major',                'id': 22, 'points': 20001},
	{'name': 'Major*',               'id': 23, 'points': 25001},
	{'name': 'Major**',              'id': 24, 'points': 31001},
	{'name': 'Major***',             'id': 25, 'points': 40001},
	{'name': 'Commander',            'id': 26, 'points': 52001},
	{'name': 'Commander*',           'id': 27, 'points': 67001},
	{'name': 'Commander**',          'id': 28, 'points': 85001},
	{'name': 'Commander***',         'id': 29, 'points': 110001},
	{'name': 'Lt Colonel',           'id': 30, 'points': 140001},
	{'name': 'Lt Colonel*',          'id': 31, 'points': 180001},
	{'name': 'Lt Colonel**',         'id': 32, 'points': 225001},
	{'name': 'Lt Colonel***',        'id': 33, 'points': 285001},
	{'name': 'Colonel',              'id': 34, 'points': 355001},
	{'name': 'Colonel*',             'id': 35, 'points': 435001},
	{'name': 'Colonel**',            'id': 36, 'points': 540001},
	{'name': 'Colonel***',           'id': 37, 'points': 660001},
	{'name': 'General',              'id': 38, 'points': 800001},
	{'name': 'General*',             'id': 39, 'points': 950001},
	{'name': 'General**',            'id': 40, 'points': 1140001},
	{'name': 'General***',           'id': 41, 'points': 1350001},
	{'name': 'Field Marshal',        'id': 42, 'points': 1600001},
	{'name': 'Field Marshal*',       'id': 43, 'points': 1875001},
	{'name': 'Field Marshal**',      'id': 44, 'points': 2185001},
	{'name': 'Field Marshal***',     'id': 45, 'points': 2550001},
	{'name': 'Supreme Marshal',      'id': 46, 'points': 3000001},
	{'name': 'Supreme Marshal*',     'id': 47, 'points': 3500000},
	{'name': 'Supreme Marshal**',    'id': 48, 'points': 4150001},
	{'name': 'Supreme Marshal***',   'id': 49, 'points': 4900001},
	{'name': 'National Force',       'id': 50, 'points': 5800001},
	{'name': 'National Force*',      'id': 51, 'points': 7000001},
	{'name': 'National Force**',     'id': 52, 'points': 9000001},
	{'name': 'National Force***',    'id': 53, 'points': 11500001},
	{'name': 'World Class Force',    'id': 54, 'points': 14500001},
	{'name': 'World Class Force*',   'id': 55, 'points': 18000001},
	{'name': 'World Class Force**',  'id': 56, 'points': 22000001},
	{'name': 'World Class Force***', 'id': 57, 'points': 26500001},
	{'name': 'Legendary Force',      'id': 58, 'points': 31500001},
	{'name': 'Legendary Force*',     'id': 59, 'points': 37000001},
	{'name': 'Legendary Force**',    'id': 60, 'points': 43000001},
	{'name': 'Legendary Force***',   'id': 61, 'points': 50000001},
	{'name': 'God of War',           'id': 62, 'points': 100000001},
	{'name': 'God of War*',          'id': 63, 'points': 200000001},
	{'name': 'God of War**',         'id': 64, 'points': 500000001},
	{'name': 'God of War***',        'id': 65, 'points': 90000000001}
]

__domains = {
	'constructions': {'name': 'Constructions'},
	'manufacturing': {'name': 'Manufacturing'},
	'land'         : {'name': 'Land'}
}

__industries = (
	{'id': 1,  'market_id': 1,  'name': 'Food',            'short': 'food', 'raw_material': 'Grain', 'domain': __domains['manufacturing']},
	{'id': 2,  'market_id': 2,  'name': 'Weapons',         'short': 'weapons',  'raw_material': 'Iron', 'domain': __domains['manufacturing']},
	{'id': 3,  'market_id': 3,  'name': 'Moving Tickets',  'short': 'moving-tickets', 'raw_material': 'Oil', 'domain': __domains['manufacturing']},
	{'id': 4,  'market_id': 4,  'name': 'Housing',         'short': 'house', 'raw_material': 'Stone', 'domain': __domains['constructions']},
	{'id': 5,  'market_id': 5,  'name': 'Hospital',        'short': 'hospital', 'raw_material': 'Stone', 'domain': __domains['constructions']},
	{'id': 6,  'market_id': 6,  'name': 'Defense Systems', 'short': 'defense-system', 'raw_material': 'Stone', 'domain': __domains['constructions']},
	{'id': 7,  'market_id': 7,  'name': 'Grain',           'short': 'grain',     'domain': __domains['land']},
	{'id': 8,  'market_id': 7,  'name': 'Fruits',          'short': 'fruits',    'domain': __domains['land']},
	{'id': 9,  'market_id': 7,  'name': 'Fish',            'short': 'fish',      'domain': __domains['land']},
	{'id': 10, 'market_id': 7,  'name': 'Cattle',          'short': 'cattle',    'domain': __domains['land']},
	{'id': 11, 'market_id': 7,  'name': 'Deer',            'short': 'deer',      'domain': __domains['land']},
	{'id': 12, 'market_id': 12, 'name': 'Iron',            'short': 'iron',      'domain': __domains['land']},
	{'id': 13, 'market_id': 12, 'name': 'Oil',             'short': 'oil',       'domain': __domains['land']},
	{'id': 14, 'market_id': 12, 'name': 'Aluminum',        'short': 'aluminum',  'domain': __domains['land']},
	{'id': 15, 'market_id': 12, 'name': 'Saltpeter',       'short': 'saltpeter', 'domain': __domains['land']},
	{'id': 16, 'market_id': 12, 'name': 'Rubber',          'short': 'rubber',    'domain': __domains['land']},
	{'id': 7,  'market_id': 7,  'name': 'Food Raw Material', 'short': 'food-raw-material', 'domain': __domains['land']},
	{'id': 12, 'market_id': 12, 'name': 'Weapon Raw Material', 'short': 'weapon-raw-material', 'domain': __domains['land']}
)

class InvalidValueError(Exception):
	def __init__(self, message):
		self.msg = message

	def __str__(self):
		return str(self.msg)

def fight_calc(firepower, rank, strength, natural_enemy=False):
	q_mult = 1 + Decimal(firepower) / 100
	r_mult = Decimal(rank['id'])
	s_mult = Decimal(strength)
	inf = (((r_mult-1)/20 + Decimal('0.3')) * ((s_mult / 10) + 40)) * q_mult
	if natural_enemy:
		inf = inf + inf * Decimal('0.1')
	return inf.to_integral_value(ROUND_DOWN)

def get_rank(rank_name):
	if rank_name == None:
		return __ranks[0]

	rank_name = rank_name.lower()
	
	for rank in __ranks:
		if rank_name == rank['name'].lower() or rank_name == str(rank['id']):
			return rank

	raise InvalidValueError('%s is not a valid military rank' % rank_name)

def get_rank_by_points(rank_points):
	i = 0
	while rank_points > __ranks[i]['points']:
		i += 1
	else:
		return __ranks[i]
	
def get_domain(val):
	if isinstance(val, str) or isinstance(val, unicode):
		if val in __domains:
			return __domains[val]

		for x in __domains:
			if val.lower() in __domains[x]['name'].lower():
				return __domains[x]

	raise InvalidValueError('%s is not a valid domain' % val)

def get_industry(val):
	val = val.lower() if not isinstance(val, dict) else val['name'].lower()

	for industry in __industries:
		if val in industry['short'] or val in industry['name'].lower():
			return industry

	raise InvalidValueError('%s is not a valid industry' % val)

def get_quality(val):
	try:
		val = int(val)
	except:
		raise InvalidValueError('%s is not a valid quality' % val)

	if val < 0 or val > 5:
		raise InvalidValueError('%d is not a valid quality.' % val)

	return val
