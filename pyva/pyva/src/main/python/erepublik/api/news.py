import feed
from feed import XmlFeed
from datetime import datetime

def get():
	return News(XmlFeed('http://www.erepublik.com/rss/allMilitaryEvents', {'n': 'http://www.w3.org/2005/Atom'}))

class News:
	"""Erepublik military events"""
	
	def __init__(self, f):
		self.updated = datetime.strptime(f.text('/n:feed/n:updated'), '%Y-%m-%dT%H:%M:%SZ').timetuple()
		self.entries = [{
			'title': entry.text('n:title'),
			'link': entry.text('n:link'),
			'updated': datetime.strptime(entry.text('n:updated'), '%Y-%m-%dT%H:%M:%SZ').timetuple(),
			'id': entry.text('n:id'),
		}
		for entry in f.elements('/n:feed/n:entry')]

