import urllib
from StringIO import StringIO
from lxml import etree
from feed import XmlFeed, HtmlFeed


class IrpgPlayer(object):
	def __init__(self, name):
		url = 'http://idlerpg.rizon.net/xml.php?'
		url += urllib.urlencode({'player': name})

		# we do our own url -> XML document translation here
		#  because we need to replace bad irc control characters with something we cn actually handle
		feed = HtmlFeed(url)
		# we'll do replacements and print out the right values later, when we fix bare @ handling (UModule): .replace(chr(2), '@b').replace(chr(3), '@c').replace(chr(15), '@o').replace(chr(31), '@u')
		feed_data = feed.html().replace(chr(2), '').replace(chr(3), '').replace(chr(15), '').replace(chr(31), '')
		element = etree.parse(StringIO(feed_data)).getroot()

		xml = XmlFeed(element)
		xml = xml.elements('/player')[0]
		self.name = xml.text('username')
		self.is_admin = xml.bool('isadmin')
		self.level = xml.int('level')
		self.classe = xml.text('class')
		self.ttl = xml.int('ttl')
		self.idled_for = xml.int('totalidled')
		self.is_online = xml.bool('online')
		alignment = xml.text('alignment')
		if alignment == 'g':
			self.alignment = 'Good'
		elif alignment == 'e':
			self.alignment = 'Evil'
		else:
			self.alignment = 'Neutral'
