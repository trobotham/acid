import urllib
from feed import get_json

class Imdb(object):
	def __init__(self, api_key):
		self.api_key = api_key
		#TODO: cache?
	
	def get(self, title):
		url = 'http://www.omdbapi.com/?y=&plot=short&r=json&' +\
		urllib.urlencode({'apikey': self.api_key}) + '&' +\
		urllib.urlencode({'t': title})
		return get_json(url)
