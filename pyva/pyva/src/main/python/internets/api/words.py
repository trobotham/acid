from wordnik import *
import urllib

class Words(object):
	apiUrl = "http://api.wordnik.com/v4"

	def __init__(self, key):
		self.wordnik = WordApi.WordApi(swagger.ApiClient(key, self.apiUrl))
		self.last_def = {}
	
	def definition(self, word):
		word = urllib.quote_plus(word)
		if word in self.last_def:
			return self.last_def[word]
		
		defs = self.wordnik.getDefinitions(word, sourceDictionaries='ahd', useCanonical=True, includeTags=False)
		
		self.last_def = {word: defs}
		return defs
