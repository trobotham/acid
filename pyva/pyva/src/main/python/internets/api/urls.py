import urllib
import requests
from feed import get_json

class Urls(object):
	def __init__(self, bitly_user, bitly_key):
		self.bitly_user = bitly_user
		self.bitly_key = bitly_key
	
	def shorten(self, long_url):
		url = 'http://api.bitly.com/v3/shorten?login=%s&apiKey=%s&format=json&' % (self.bitly_user, self.bitly_key)
		url += urllib.urlencode({'longUrl': long_url})
		reply = get_json(url)
		return reply
	
	def expand(self, short_url):
		r = requests.head(short_url)
		if r.status_code // 100 in (2, 3): # status_code is 2XX or 3XX
			return {'long-url': r.headers['location']}
		elif r.status_code // 100 == 4:
			return {'error': 'invalid shortened url.'}
		else:
			return {'error': 'server busy or connection time out.'}
