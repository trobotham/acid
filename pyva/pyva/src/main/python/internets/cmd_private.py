from pseudoclient.cmd_manager import *
from datetime import datetime

def private_channel_request(self, manager, opts, arg, channel, sender, userinfo):
	if self.channels.is_valid(arg):
		self.msg(channel, "I'm already in @b%s@b." % arg)
	elif self.channels.is_banned(arg):
		chan = self.channels[arg]
		self.elog.request('Request for banned channel @b%s@b from @b%s@b.' % (arg, sender, userinfo))
		message = 'Request failed, channel @b%s@b was banned by @b%s@b.' % (arg, chan.ban_source)

		if chan.ban_reason != None:
			message += ' Reason: @b%s@b.' % chan.ban_reason

		if chan.ban_expiry != None:
			message += ' Expires: @b%s@b.' % datetime.fromtimestamp(chan.ban_expiry)

		self.msg(channel, message)
	else:
		self.auth.request(sender, arg, 'request')

def private_channel_remove(self, manager, opts, arg, channel, sender, userinfo):
	if not self.channels.is_valid(arg):
		self.msg(channel, "I'm not in @b%s@b." % arg)
	else:
		self.auth.request(sender, arg, 'remove')

def private_channel_ytinfo_enable(self, manager, opts, arg, channel, sender, userinfo):
	if not self.channels.is_valid(arg):
		self.msg(channel, "I'm not in @b%s@b." % arg)
	else:
		self.auth.request(sender, arg, 'ytinfo')

def private_channel_ytinfo_disable(self, manager, opts, arg, channel, sender, userinfo):
	if not self.channels.is_valid(arg):
		self.msg(channel, "I'm not in @b%s@b." % arg)
	else:
		self.auth.request(sender, arg, 'noytinfo')

def private_help(self, manager, opts, arg, channel, sender, userinfo):
	command = arg.lower()

	if command == '':
		message = manager.get_help()
	else:
		message = manager.get_help(command)

		if message == None:
			message = ['%s is not a valid command.' % arg]

	for line in message:
		self.msg(channel, line)


class PrivateCommandManager(CommandManager):
	def get_prefix(self):
		return ''

	def get_invalid(self):
		return 'Invalid message. Say help for a list of valid messages.'

	def get_commands(self):
		return {
			'request': (private_channel_request, ARG_YES|ARG_OFFLINE, 'requests a channel (must be founder)', [], '#channel'),
			'remove': (private_channel_remove, ARG_YES|ARG_OFFLINE, 'removes a channel (must be founder)', [], '#channel'),
			'ytinfo': (private_channel_ytinfo_enable, ARG_YES|ARG_OFFLINE, 'enables automatic YouTube URL information lookup (must be founder)', [], '#channel'),
			'noytinfo': (private_channel_ytinfo_disable, ARG_YES|ARG_OFFLINE, 'disables automatic YouTube URL information lookup (must be founder)', [], '#channel'),
			'hi': 'help',
			'hello': 'help',
			'help': (private_help, ARG_OPT, 'displays help text', []),
		}
